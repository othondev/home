#!/bin/bash

ln -sf $PWD/tmux.conf ~/.tmux.conf
ln -sf $PWD/tmux ~/.tmux
ln -sf $PWD/zshrc ~/.zshrc
ln -sf $PWD/zsh ~/.zsh

