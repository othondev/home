export ZSH=$HOME/.oh-my-zsh
ZSH_THEME="spaceship"
plugins=(
  git
  zsh-syntax-highlighting
  k
)

[[ -z "$TMUX" ]] && tmux a

source $HOME/.dotfiles/zsh/aliases/*
source $ZSH/oh-my-zsh.sh
